#!/bin/sh -e

## Helper script for starting a system daemon.
## Used in systemd unit file.
##
## Redirects all daemon's output to the rotated log file within /var/log.

/usr/sbin/fpo-syncer 2>&1 | /usr/bin/reopener -s /var/log/fpo/fpo-syncer/messages.log
