.PHONY: all link build clean

all: build

link: clean

build: link
	GOPATH=${PWD}/vendor:/usr/share/gocode go build -o \
		${PWD}/bin/fpo-syncer ./src/main.go

clean:
	rm -f ${PWD}/bin/fpo-syncer
