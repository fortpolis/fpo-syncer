package syncer

import (
	log "fpo_commons/log"
	"fpo_commons/mbuscall"
	"fpo_types"

	"gopkg.in/mgo.v2/bson"
)

func DMarks() {
	// get dictionary for sync
	dictionary := fpo_types.Dictionary{}
	resp := mbuscall.Response{}
	err := mbuscall.Call("fort.db.dictionaries.get",
		&map[string]interface{}{
			"query": bson.M{
				"cdb_alias": "dmarks",
			},
		},
		&resp,
		&dictionary)
	if err != nil {
		log.Log("[dmarks] error while sync: %s", err)
		return
	}

	if dictionary.ID == "" {
		newDMarksRoutine()
		return
	}
	updateDMarksRoutine(dictionary.ID)
}

func updateDMarksRoutine(dictID fpo_types.FortID) {
	log.Log("[dmarks] started dmarks update routine")

	dmarks := getDMarksChanges()
	if dmarks == nil {
		log.Log("[dmarks] error while")
		return
	}

	log.Log("[dmarks] len on new entities: %d", len(dmarks.Response))

	for _, i := range dmarks.Response {
		if i.IsActive == "0" {
			continue
		}

		switch i.TypeOp {
		case "I":
			resp := mbuscall.Response{}
			err := mbuscall.Call("fort.db.dictionaries.value.put",
				&map[string]interface{}{
					"body": fpo_types.DictionaryValue{
						DictionaryID: dictID,
						Value:        i.ID,
						Title:        i.Name,
						Synonims:     []string{i.NameRu, i.NameEn},
					},
				},
				&resp,
				nil)
			if err != nil {
				log.Log("[dmarks] error while creation new dict: %s", err)
				return
			}
			continue
		case "M":
			value := fpo_types.DictionaryValue{}
			resp := mbuscall.Response{}
			err := mbuscall.Call("fort.db.dictionaries.value.get",
				&map[string]interface{}{
					"query": map[string]interface{}{
						"value": i.ID,
					},
				},
				&resp,
				&value)
			if err != nil {
				log.Log("[dmarks] error while cdb interaction: %s", err)
				return
			}

			value.Value = i.ID
			value.Title = i.Name
			value.Synonims = []string{i.NameRu, i.NameEn}

			resp = mbuscall.Response{}
			err = mbuscall.Call("fort.db.dictionaries.value.patch",
				&map[string]interface{}{
					"_id":  value.ID.Hex(),
					"body": value,
				},
				&resp,
				&value)
			if err != nil {
				log.Log("[dmarks] error while cdb interaction: %s", err)
				return
			}
		}
	}
}

func newDMarksRoutine() {
	log.Log("[dmarks] started dmarks create routine")

	dmarks := getDMarks()
	if dmarks == nil {
		log.Log("[dmarks] error while")
		return
	}

	dmarksDict := fpo_types.Dictionary{
		Title:    "Довідник марок авто",
		CDBAlias: "dmarks",
	}
	dmarksDict.ID = fpo_types.NewFortID()

	vals := []fpo_types.DictionaryValue{}
	for _, i := range dmarks.Response {
		if i.IsActive == "0" {
			continue
		}

		vals = append(vals, fpo_types.DictionaryValue{
			DictionaryID: dmarksDict.ID,
			Value:        i.ID,
			Title:        i.Name,
			Synonims:     []string{i.NameRu, i.NameEn},
		})
	}

	dmarksDict.Values = vals

	resp := mbuscall.Response{}
	err := mbuscall.Call("fort.db.dictionaries.put",
		&map[string]interface{}{
			"body": dmarksDict,
		},
		&resp,
		nil)
	if err != nil {
		log.Log("[dmarks] error while creation new dict: %s", err)
		return
	}
}
