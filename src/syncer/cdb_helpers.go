package syncer

import (
	log "fpo_commons/log"
	"fpo_commons/mbuscall"
	"fpo_types/cdb"
	"time"
)

func getDMarks() *cdb.MarksResponse {
	data := cdb.MarksResponse{}
	resp := mbuscall.Response{}
	err := mbuscall.Call("fpo.cdb.dict.marks",
		&map[string]interface{}{},
		&resp,
		&data)
	if err != nil {
		log.Log("[dmarks] error while cdb interaction: %s", err)
		return nil
	}
	return &data
}

func getDMarksChanges() *cdb.MarksResponse {
	data := cdb.MarksResponse{}

	resp := mbuscall.Response{}
	err := mbuscall.Call("fpo.cdb.dict.marks.changes",
		&map[string]interface{}{
			"body": map[string]interface{}{
				"from": syncMap[TDMarks],
				"to":   time.Now(),
			},
		},
		&resp,
		&data)
	if err != nil {
		log.Log("[dmarks] error while cdb interaction: %s", err)
		return nil
	}
	return &data
}

func getDModels() *cdb.ModelsResponse {
	data := cdb.ModelsResponse{}
	resp := mbuscall.Response{}
	err := mbuscall.Call("fpo.cdb.dict.models",
		&map[string]interface{}{},
		&resp,
		&data)
	if err != nil {
		log.Log("[dmodels] error while cdb interaction: %s", err)
		return nil
	}
	return &data
}

func getDModelsChanges() *cdb.ModelsResponse {
	data := cdb.ModelsResponse{}

	resp := mbuscall.Response{}
	err := mbuscall.Call("fpo.cdb.dict.models.changes",
		&map[string]interface{}{
			"body": map[string]interface{}{
				"from": syncMap[TDMarks],
				"to":   time.Now(),
			},
		},
		&resp,
		&data)
	if err != nil {
		log.Log("[dmodels] error while cdb interaction: %s", err)
		return nil
	}
	return &data
}

func getDCity() *cdb.DCityResponse {
	data := cdb.DCityResponse{}
	resp := mbuscall.Response{}
	err := mbuscall.Call("fpo.cdb.dict.cities",
		&map[string]interface{}{},
		&resp,
		&data)
	if err != nil {
		log.Log("[dcity] error while cdb interaction: %s", err)
		return nil
	}
	return &data
}

func getDCityChanges() *cdb.DCityResponse {
	data := cdb.DCityResponse{}

	resp := mbuscall.Response{}
	err := mbuscall.Call("fpo.cdb.dict.cities.changes",
		&map[string]interface{}{
			"body": map[string]interface{}{
				"from": syncMap[TDMarks],
				"to":   time.Now(),
			},
		},
		&resp,
		&data)
	if err != nil {
		log.Log("[dcity] error while cdb interaction: %s", err)
		return nil
	}
	return &data
}
