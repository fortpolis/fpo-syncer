package syncer

import (
	log "fpo_commons/log"
	"fpo_commons/mbuscall"
	"fpo_types"

	"gopkg.in/mgo.v2/bson"
)

func DCity() {
	// get dictionary for sync
	dictionary := fpo_types.Dictionary{}
	resp := mbuscall.Response{}
	err := mbuscall.Call("fort.db.dictionaries.get",
		&map[string]interface{}{
			"query": bson.M{
				"cdb_alias": "dcity",
			},
		},
		&resp,
		&dictionary)
	if err != nil {
		log.Log("[dcity] error while sync: %s", err)
		return
	}

	if dictionary.ID == "" {
		newDCityRoutine()
		return
	}
	updateDCityRoutine(dictionary.ID)
}

func updateDCityRoutine(dictID fpo_types.FortID) {
	log.Log("[dcity] started dcity update routine")

	dcity := getDCityChanges()
	if dcity == nil {
		log.Log("[dcity] error while")
		return
	}

	log.Log("[dcity] len on new entities: %d", len(dcity.Response))

	for _, i := range dcity.Response {
		if i.IsActive == "0" {
			continue
		}

		switch i.TypeOp {
		case "I":
			resp := mbuscall.Response{}
			err := mbuscall.Call("fort.db.dictionaries.value.put",
				&map[string]interface{}{
					"body": fpo_types.DictionaryValue{
						DictionaryID: dictID,
						Value:        i.ID,
						Title:        i.Name,
						Synonims:     []string{i.RegistryID},
						Comment:      "ZoneCode: " + i.ZoneCode,
					},
				},
				&resp,
				nil)
			if err != nil {
				log.Log("[dcity] error while creation new dict: %s", err)
				return
			}
			continue
		case "M":
			value := fpo_types.DictionaryValue{}
			resp := mbuscall.Response{}
			err := mbuscall.Call("fort.db.dictionaries.value.get",
				&map[string]interface{}{
					"query": map[string]interface{}{

						"value": i.ID,
					},
				},
				&resp,
				&value)
			if err != nil {
				log.Log("[dcity] error while cdb interaction: %s", err)
				return
			}

			value.Value = i.ID
			value.Title = i.Name
			value.Synonims = []string{i.RegistryID}
			value.Comment = "ZoneCode: " + i.ZoneCode

			resp = mbuscall.Response{}
			err = mbuscall.Call("fort.db.dictionaries.value.patch",
				&map[string]interface{}{
					"_id":  value.ID.Hex(),
					"body": value,
				},
				&resp,
				&value)
			if err != nil {
				log.Log("[dcity] error while cdb interaction: %s", err)
				return
			}
		}
	}
}

func newDCityRoutine() {
	dcity := getDCity()
	if dcity == nil {
		log.Log("[dcity] error while")
		return
	}

	dcityDict := fpo_types.Dictionary{
		Title:    "Довідник населений пунктів МРЕО",
		CDBAlias: "dcity",
	}
	dcityDict.ID = fpo_types.NewFortID()

	vals := []fpo_types.DictionaryValue{}
	for _, i := range dcity.Response {
		if i.IsActive == "0" {
			continue
		}

		vals = append(vals, fpo_types.DictionaryValue{
			DictionaryID: dcityDict.ID,
			Value:        i.ID,
			Title:        i.Name,
			Synonims:     []string{i.RegistryID},
			Comment:      "ZoneCode: " + i.ZoneCode,
		})
	}

	dcityDict.Values = vals

	resp := mbuscall.Response{}
	err := mbuscall.Call("fort.db.dictionaries.put",
		&map[string]interface{}{
			"body": dcityDict,
		},
		&resp,
		nil)
	if err != nil {
		log.Log("[dcity] error while creation new dict: %s", err)
		return
	}
}
