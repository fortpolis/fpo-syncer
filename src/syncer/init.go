package syncer

import "time"

const (
	TDCity = iota
	TDMarks
	TDModels
)

var syncMap = map[int]time.Time{
	TDCity:   time.Now(),
	TDMarks:  time.Now(),
	TDModels: time.Now(),
}

func init() {}
