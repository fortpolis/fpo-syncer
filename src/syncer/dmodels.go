package syncer

import (
	log "fpo_commons/log"
	"fpo_commons/mbuscall"
	"fpo_types"

	"gopkg.in/mgo.v2/bson"
)

func DModels() {
	dictionary := fpo_types.Dictionary{}
	resp := mbuscall.Response{}
	err := mbuscall.Call("fort.db.dictionaries.get",
		&map[string]interface{}{
			"query": bson.M{
				"cdb_alias": "dmodels",
			},
		},
		&resp,
		&dictionary)
	if err != nil {
		log.Log("[dmodels] error while sync: %s", err)
		return
	}

	if dictionary.ID == "" {
		newDModelsRoutine()
		return
	}
	updateDModelsRoutine(dictionary.ID)
}

func newDModelsRoutine() {
	log.Log("[dmodels] started dmarks create routine")

	dmarks := getDModels()
	if dmarks == nil {
		log.Log("[dmodels] error while")
		return
	}

	log.Log("[dmodels] count of new models: %d", len(dmarks.Response))

	dMarksDict := getDMarkDict()

	dict := fpo_types.Dictionary{
		Title:      "Довідник моделей авто",
		LinkedWith: &dMarksDict.ID,
		CDBAlias:   "dmodels",
	}
	dict.ID = fpo_types.NewFortID()

	resp := mbuscall.Response{}
	err := mbuscall.Call("fort.db.dictionaries.put",
		&map[string]interface{}{
			"body": dict,
		},
		&resp,
		nil)
	if err != nil {
		log.Log("[dmodels] error while creation new dict: %s", err)
		return
	}

	for _, i := range dmarks.Response {
		if i.IsActive == "0" {
			continue
		}

		modelID := getDMarkIDByCode(i.MarkID, dMarksDict.ID.Hex())
		if modelID == "" {
			log.Log("[dmodels] cant resolve mark for model: %#v, dmark dict id: %s",
				i,
				dMarksDict.ID.Hex())
			continue
		}

		value := fpo_types.DictionaryValue{
			DictionaryID: dict.ID,
			LinkedValue:  modelID,
			Value:        i.ID,
			Title:        i.Name,
			Synonims:     []string{i.NameRu, i.NameEn},
		}
		resp = mbuscall.Response{}
		err = mbuscall.Call("fort.db.dictionaries.value.put",
			&map[string]interface{}{
				"body": value,
			},
			&resp,
			&value)
		if err != nil {
			log.Log("[dmodels] error while cdb interaction: %s", err)
			return
		}
	}
}

func updateDModelsRoutine(dictID fpo_types.FortID) {
	log.Log("[dmodels] started dmarks update routine")

	dmodels := getDModelsChanges()
	if dmodels == nil {
		log.Log("[dmodels] error while")
		return
	}

	dMarksDict := getDMarkDict()

	log.Log("[dmodels] len on new entities: %d", len(dmodels.Response))

	for _, i := range dmodels.Response {
		if i.IsActive == "0" {
			continue
		}

		switch i.TypeOp {
		case "I":
			modelID := getDMarkIDByCode(i.MarkID, dMarksDict.ID.Hex())
			if modelID == "" {
				log.Log("[dmodels] cant resolve mark for model: %#v, dmark dict id: %s",
					i,
					dMarksDict.ID.Hex())
				continue
			}

			resp := mbuscall.Response{}
			err := mbuscall.Call("fort.db.dictionaries.value.put",
				&map[string]interface{}{
					"body": fpo_types.DictionaryValue{
						DictionaryID: dictID,
						LinkedValue:  modelID,
						Value:        i.ID,
						Title:        i.Name,
						Synonims:     []string{i.NameRu, i.NameEn},
					},
				},
				&resp,
				nil)
			if err != nil {
				log.Log("[dmodels] error while creation new dict: %s", err)
				return
			}
			continue
		case "M":
			value := fpo_types.DictionaryValue{}
			resp := mbuscall.Response{}
			err := mbuscall.Call("fort.db.dictionaries.value.get",
				&map[string]interface{}{
					"query": map[string]interface{}{
						"value": i.ID,
					},
				},
				&resp,
				&value)
			if err != nil {
				log.Log("[dmodels] error while cdb interaction: %s", err)
				return
			}

			value.Value = i.ID
			value.Title = i.Name
			value.Synonims = []string{i.NameRu, i.NameEn}

			resp = mbuscall.Response{}
			err = mbuscall.Call("fort.db.dictionaries.value.patch",
				&map[string]interface{}{
					"_id":  value.ID.Hex(),
					"body": value,
				},
				&resp,
				&value)
			if err != nil {
				log.Log("[dmodels] error while cdb interaction: %s", err)
				return
			}
		}
	}
}

func getDMarkIDByCode(code string, dictID string) fpo_types.FortID {
	value := fpo_types.DictionaryValue{}
	resp := mbuscall.Response{}
	err := mbuscall.Call("fort.db.dictionaries.value.get",
		&map[string]interface{}{
			"query": map[string]interface{}{
				"value":         code,
				"dictionary_id": dictID,
			},
		},
		&resp,
		&value)
	if err != nil {
		log.Log("[dmarks] error while cdb interaction: %s", err)
		return ""
	}
	return value.ID
}

func getDMarkDict() *fpo_types.Dictionary {
	dDmarksDict := fpo_types.Dictionary{}
	resp := mbuscall.Response{}
	err := mbuscall.Call("fort.db.dictionaries.get",
		&map[string]interface{}{
			"query": bson.M{
				"cdb_alias": "dmarks",
			},
		},
		&resp,
		&dDmarksDict)
	if err != nil {
		log.Log("[dmodels] error while sync: %s", err)
		return &dDmarksDict
	}
	return &dDmarksDict
}
