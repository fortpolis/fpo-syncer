package main

import (
	"fpo_commons/config"
	log "fpo_commons/log"

	"./syncer"

	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	var stop = make(chan os.Signal)
	signal.Notify(stop, syscall.SIGTERM)
	signal.Notify(stop, syscall.SIGINT)

	ticker := time.NewTicker(config.GetDurationFromEnvDef("FPO_SYNC_DURATION",
		24*time.Hour))
	quit := make(chan struct{})

	go func() {
		process()
		for {
			select {
			case <-ticker.C:
				process()

			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()

	sig := <-stop
	log.Log("sync server is down after: %+v", sig)
}

func process() {
	go syncer.DCity()

	go func() {
		syncer.DMarks()
		syncer.DModels()
	}()

}
